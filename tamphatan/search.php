<?php
/**
 * The Template for displaying Search Results pages.
 */

get_header();

?>	
	<div class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="inner">
						<h1><?php printf( esc_html__( 'Kết quả tìm kiếm cho từ khóa: %s', 'tamphatan' ), get_search_query() ); ?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	echo '<div class="container mt-5">';
		if ( have_posts() ) :
		
			get_template_part( 'archive', 'loop' );

		else :
			get_template_part( 'content', 'none' );

		endif;

		wp_reset_postdata(); 
	echo '</div>';

get_footer();
