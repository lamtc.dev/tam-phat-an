<?php
/**
 * The template for displaying search forms
 */
?>
<form class="search-form" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="row">
		<div class="col-12">
			<div class="input-group">
				<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'Nhập từ khóa tìm kiếm...', 'tamphatan' ); ?>" />
				<div class="input-group-append">
					<button type="submit" class="btn btn-primary" name="submit"><?php esc_html_e( 'Tìm kiếm', 'tamphatan' ); ?></button>
				</div><!-- /.input-group-append -->
			</div><!-- /.input-group -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</form>
