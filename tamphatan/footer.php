		</main><!-- /#main -->
		<section class="w-cta w-block-content">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="business-info">
							<?php if ($GLOBALS['global_cty_name']) {echo '<h3>'.$GLOBALS['global_cty_name'].'</h3>';} ?>
							<?php if ($GLOBALS['global_cty_add']) {echo '<p> Địa chỉ: '.$GLOBALS['global_cty_add'].'</p>';} ?>
							<?php if ($GLOBALS['global_hotline']) {echo '<p> Số điện thoại: '.$GLOBALS['global_hotline'].'</p>';} ?>
							<?php if ($GLOBALS['global_email']) {echo '<p> Địa chỉ Email: '.$GLOBALS['global_email'].'</p>';} ?>
							<p> Website: <?= get_site_url() ?></p>
						</div>
					</div>
					<div class="col-md-6">
						<?= do_shortcode( '[contact-form-7 id="193" title="Form gửi yêu cầu"]' ) ?>
					</div>
				</div>
			</div>
		</section>
		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p><?php printf( esc_html__( '&copy; %1$s %2$s. All rights reserved.', 'tamphatan' ), date_i18n( 'Y' ), get_bloginfo( 'name', 'display' ) ); ?> <?= ($GLOBALS['global_cty_cert']) ? $GLOBALS['global_cty_cert'] : '' ?></p>
					</div>

					<?php
						if ( has_nav_menu( 'footer-menu' ) ) : // see function register_nav_menus() in functions.php
							/*
								Loading WordPress Custom Menu (theme_location) ... remove <div> <ul> containers and show only <li> items!!!
								Menu name taken from functions.php!!! ... register_nav_menu( 'footer-menu', 'Footer Menu' );
								!!! IMPORTANT: After adding all pages to the menu, don't forget to assign this menu to the Footer menu of "Theme locations" /wp-admin/nav-menus.php (on left side) ... Otherwise the themes will not know, which menu to use!!!
							*/
							wp_nav_menu(
								array(
									'theme_location'  => 'footer-menu',
									'container'       => 'nav',
									'container_class' => 'col-md-6',
									'fallback_cb'     => '',
									'items_wrap'      => '<ul class="menu nav justify-content-end">%3$s</ul>',
									//'fallback_cb'    => 'WP_Bootstrap4_Navwalker_Footer::fallback',
									'walker'          => new WP_Bootstrap4_Navwalker_Footer(),
								)
							);
						endif;

						if ( is_active_sidebar( 'third_widget_area' ) ) :
					?>
						<div class="col-md-12">
							<?php
								dynamic_sidebar( 'third_widget_area' );

								if ( current_user_can( 'manage_options' ) ) :
							?>
								<span class="edit-link"><a href="<?php echo esc_url( admin_url( 'widgets.php' ) ); ?>" class="badge badge-secondary"><?php esc_html_e( 'Edit', 'tamphatan' ); ?></a></span><!-- Show Edit Widget link -->
							<?php
								endif;
							?>
						</div>
					<?php
						endif;
					?>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</footer><!-- /#footer -->
	</div><!-- /#wrapper -->
	<?php
		wp_footer();
	?>
	<?php
	if (get_field('code_before_body_close', 'option'))
		the_field('code_before_body_close', 'option');
	?>
</body>
</html>
