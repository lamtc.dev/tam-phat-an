<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="inner">
                    <h1><?= (is_category() || is_archive() ) ? single_cat_title( '', false ) : get_the_title() ?></h1>
                    <?= the_breadcrumb() ?>
                </div>
            </div>
        </div>
    </div>
</div>