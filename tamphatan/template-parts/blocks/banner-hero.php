<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'w-hero';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}

// Load values and assign defaults.
$items = get_field('items');
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <!-- Dots -->
        <?php if( have_rows('items') ): $i = 0;?>
            <ol class="carousel-indicators">
                <?php while( have_rows('items') ): the_row(); ?>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>" <?php if($i == 0) {echo 'class="active"';} ?>></li>
                <?php $i++; endwhile; ?>
            </ol>
        <?php endif; ?>
        <?php if( have_rows('items') ): $j = 0; ?>
            <div class="carousel-inner">
                <?php while( have_rows('items') ): the_row(); $image = get_sub_field('img'); $caption = get_sub_field('text'); ?>
                    <div class="carousel-item <?= ($j == 0) ? 'active' : '' ?>">
                        <img src="<?php echo esc_url($image['url']); ?>" class="d-block w-100" alt="<?php echo esc_attr($image['alt']); ?>">
                        <?php if($caption) : ?>
                            <div class="carousel-caption d-none d-md-block">
                                <?php echo $caption; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php $j++; endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>