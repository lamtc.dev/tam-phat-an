<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'w-clients w-block-content';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}

// Load values and assign defaults.
$title = get_field('title');
$items = get_field('items');
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="container">
        <div class="row row-eq-height no-gutters align-items-center">
            <div class="col-2 d-none d-lg-block">
                <?php if($title) : ?>
                    <div class="title"><?= $title ?></div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-lg-10">
                <?php if($items) : ?>
                    <div class="client-slider">
                        <?php foreach( $items as $item ): ?>
                            <div class="item">
                                <?= ($logo = $item['logo']) ? wp_get_attachment_image($logo, 'full') : '' ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>