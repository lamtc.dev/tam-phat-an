<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'w-portfolio w-block-content';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}

// Load values and assign defaults.
$title = get_field('title');
$items = get_field('items');
$button = get_field('button');
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="container">

        <?php if($title) : ?>
        <h2 class="block-title text-center">
            <span><?= $title ?></span>
        </h2>
        <?php endif; ?>

        <?php if($items) : ?>
        <div class="row mt-4 justify-content-center">
            <?php foreach( $items as $item ): ?>
                <div class="col-6 col-lg-4">
                    <article class="item box-item">
                        <a href="<?php the_permalink($item->ID); ?>" class="img-wrap link-wrap">
                            <img src="<?= get_the_post_thumbnail_url($item->ID) ?>" alt="<?php the_title(); ?>">
                        </a>
                        <a href="<?php the_permalink($item->ID); ?>">
                            <h3><?= get_the_title($item->ID); ?></h3>
                        </a>
                        <p class="decs"><?= ($except = get_the_excerpt($item->ID)) ? wp_trim_words( $except, 30, '...' ) : '' ?></p>
                    </article>
                </div>
            <?php endforeach; ?>
            <?php if($button) : ?>
                <div class="col-12 text-center">
                    <a class="btn btn-primary" href="<?= $button['url'] ?>" target="<?= $button['target'] ?>"><?= $button['title'] ?></a>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>

    </div>
</section>