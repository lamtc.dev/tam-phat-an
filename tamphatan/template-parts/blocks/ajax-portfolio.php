<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'w-ajax-portfolio';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}

// Load values and assign defaults.
$terms = get_field('portfolio_term');
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="row">
        <div class="col-12">
            <?php $i = 1; if($terms) : ?>
                <nav class="nav nav-pills nav-justified port-nav">
                    <?php foreach( $terms as $term ): ?>
                        <a data-slug="<?= esc_html($term->slug) ?>" class="nav-link <?= ($i == 1) ? 'active' : '' ?>" href="#"><?= esc_html( $term->name) ?></a>
                    <?php $i++; endforeach; ?>
                </nav>
            <?php endif; ?>
        </div>
    </div>
    <div class="row load-ports bg-white mt-4">
        <?php if($terms) : ?>
            <?php 
                $first_term_slug = $terms[0]->slug;
                $args = array(
                    'post-type' => 'du-an',
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'linh-vuc',
                            'field' => 'slug',
                            'terms' => $first_term_slug,
                        )
                    ),
                );
                // The Loop
                $query = new WP_Query( $args );        
                if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="col-md-6 col-lg-4">
                            <article class="item box-item ">
                                <a href="<?php the_permalink(); ?>" class="img-wrap link-wrap">
                                    <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title(); ?>">
                                </a>
                                <a href="<?php the_permalink(); ?>">
                                    <h3><?= get_the_title(); ?></h3>
                                </a>
                                <p class="decs"><?= ($except = get_the_excerpt()) ? wp_trim_words( $except, 30, '...' ) : '' ?></p>
                            </article>
                        </div>
                    <?php endwhile;
                endif;                       
                // Reset Post Data
                wp_reset_postdata();
            ?>
        <?php endif; ?>
    </div>
</section>
<?php 
    add_action( 'wp_footer', 'add_ajax_function' , 99, 1);
    function add_ajax_function() { ?>
        <script>
            (function($) {
                $('.port-nav .nav-link').click(function(e){
                    e.preventDefault();
                    $('.port-nav .nav-link').removeClass('active');
                    $(this).addClass('active');
                    var term_slug = $(this).data('slug');
                    console.log(term_slug);
                    $.ajax({ 
                        type : "post", 
                        dataType : "html",
                        url : '<?php echo admin_url('admin-ajax.php');?>',
                        data : {
                            action: "get_ports",
                            term_slug: term_slug, 
                        },
                        beforeSend: function(){
                            $('.load-ports').html('');
                        },
                        success: function(response) {
                            $('.load-ports').html(response); 
                        },
                        error: function( jqXHR, textStatus, errorThrown ){
                            console.log( 'The following error occured: ' + textStatus, errorThrown );
                        }
                    });
                });
            })(jQuery);
        </script> 
    <?php }
?>
