<?php

/**
 * "Banner Hero" Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tpa-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'w-whyus w-block-content';

if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$isFullWidth = false;
if( !empty($block['align']) ) {
    $isFullWidth = $block['align'] === 'full' ? true : false;
    $className .= ' align-' . $block['align'];
}

// Load values and assign defaults.
$title = get_field('title');
$items = get_field('items');
$button = get_field('button');
$default_icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-square-fill" viewBox="0 0 16 16">
<path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm10.03 4.97a.75.75 0 0 1 .011 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.75.75 0 0 1 1.08-.022z"/>
</svg>';
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="container">

        <?php if($title) : ?>
        <h2 class="block-title text-center">
            <span><?= $title ?></span>
        </h2>
        <?php endif; ?>

        <?php if($items) : ?>
        <div class="row mt-4 justify-content-center">
            <?php foreach( $items as $item ): ?>
                <div class="col-md-6">
                    <div class="list-item d-flex align-items-center">
                        <div class="icon"><?= ($icon = $item['icon']) ? $icon : $default_icon ?></div>
                        <div class="text"><?= ($text = $item['text']) ? $text : '' ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php if($button) : ?>
                <div class="col-12 text-center">
                    <a class="btn btn-primary" href="<?= $button['url'] ?>" target="<?= $button['target'] ?>"><?= $button['title'] ?></a>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>

    </div>
</section>