<?php
/**
 * The template for displaying content in the index.php template
 */
?>
<div class="col-lg-4">
	<article class="item box-item ">
		<a href="<?php the_permalink(); ?>" class="img-wrap link-wrap">
			<img src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title(); ?>">
		</a>
		<a href="<?php the_permalink(); ?>">
			<h3><?= get_the_title(); ?></h3>
		</a>
		<p class="decs"><?= ($except = get_the_excerpt()) ? wp_trim_words( $except, 30, '...' ) : '' ?></p>
	</article>
</div>

<!-- /#post-<?php the_ID(); ?> -->
