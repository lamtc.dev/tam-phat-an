<?php

add_action( 'init', 'create_posttype' );
function create_posttype() {

    register_post_type( 'du-an',
        array(
            'labels' => array(
                'name' => __( 'Dự án' ),
                'singular_name' => __( 'Dự án' )
            ),
            'public' => true,
            'has_archive' => false,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'revisions', 'trackbacks', 'author'),
            'rewrite' => array('slug' => 'du-an','with_front' => false),
            'menu_icon' => 'dashicons-admin-appearance',
            'show_in_rest'       => true,
        )
    );

}

add_action( 'init', 'register_status_tax');
function register_status_tax() {
    register_taxonomy( 'linh-vuc', 'du-an', 
        array(
            'label'        => __( 'Lĩnh vực', 'tamphatan' ),
            'public'       => true,
            'hierarchical' => true,
            'show_admin_column' => true
        )
    );
}