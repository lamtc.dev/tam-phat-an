<?php

// load_json
function acf_json_load_point($paths)
{
    // remove original path (optional)
    unset($paths[0]);

    // append path
    $paths[] = get_template_directory_uri() . '/acf-json';

    // return
    return $paths;
}
add_filter('acf/settings/load_json', 'acf_json_load_point');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Cấu hình website',
		'menu_title'	=> 'Cấu hình website',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'icon_url' => 'dashicons-superhero-alt',
	));
	
}

/**
 * Blocks.
 */
add_action('acf/init', 'tpa_acf_init_block_types');
function tpa_acf_init_block_types() {
    // Check function exists.
    if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(array(
            'name'              => 'banner-hero',
            'title'             => __('Banner Hero'),
            'description'       => __('Slider'),
            'render_template'   => 'template-parts/blocks/banner-hero.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Banner Hero', 'acf' ),
        ));
        acf_register_block_type(array(
            'name'              => 'services',
            'title'             => __('Dịch vụ'),
            'description'       => __('Hiển thị danh sách dịch vụ'),
            'render_template'   => 'template-parts/blocks/services.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Dịch vụ', 'acf' ),
        ));
        acf_register_block_type(array(
            'name'              => 'portfolio',
            'title'             => __('Dự án'),
            'description'       => __('Hiển thị các dự án đã thực hiện'),
            'render_template'   => 'template-parts/blocks/portfolio.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Dự án', 'acf' ),
        ));
        acf_register_block_type(array(
            'name'              => 'why-us',
            'title'             => __('Lý do chọn chúng tôi'),
            'description'       => __('Hiển thị các item nổi bật - lý do chọn chúng tôi'),
            'render_template'   => 'template-parts/blocks/why-us.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Lý do chọn chúng tôi', 'acf' ),
        ));
        acf_register_block_type(array(
            'name'              => 'feedback',
            'title'             => __('Ý kiến khách hàng'),
            'description'       => __('Hiển thị các feedback từ khách hàng'),
            'render_template'   => 'template-parts/blocks/feedbacks.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Ý kiến khách hàng', 'acf' ),
        ));
        acf_register_block_type(array(
            'name'              => 'clients',
            'title'             => __('Đối tác'),
            'description'       => __('Hiển thị logo các đối tác'),
            'render_template'   => 'template-parts/blocks/clients.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Đối tác', 'acf' ),
        ));
        acf_register_block_type(array(
            'name'              => 'block-news',
            'title'             => __('Tin tức'),
            'description'       => __('Hiển thị bài viết tin tức - tuyển dụng'),
            'render_template'   => 'template-parts/blocks/news.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Tin tức', 'acf' ),
        ));
        acf_register_block_type(array(
            'name'              => 'portfolio-page',
            'title'             => __('Hiển thị dự án theo lĩnh vực'),
            'description'       => __('Hiển thị dự án theo lĩnh vực'),
            'render_template'   => 'template-parts/blocks/ajax-portfolio.php',
            'category'          => 'formatting',
            'icon'              => 'block-default',
            'mode'              => 'edit',
            'align'             => 'full',
            'keywords'          => array( 'Hiển thị dự án theo lĩnh vực', 'acf' ),
        ));
    }
}