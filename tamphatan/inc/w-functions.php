<?php
/**
 *  Add Except for pages
 */
add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/**
 *  SMTP Config
 */
add_action( 'phpmailer_init', function( $phpmailer ) {

    $smtp_account = get_field('smtp_account','option');
    $mail_title = get_field('mail_title','option');
    
    if ( !is_object( $phpmailer && $smtp_account ) )
    $phpmailer = (object) $phpmailer;
    $phpmailer->Mailer     = 'smtp';
    $phpmailer->Host       = 'smtp.gmail.com';
    $phpmailer->SMTPAuth   = 1;
    $phpmailer->Port       = 587;
    $phpmailer->Username   = $smtp_account['gmail'];
    $phpmailer->Password   = $smtp_account['pass_app'];
    $phpmailer->SMTPSecure = 'TLS';
    $phpmailer->From       = $smtp_account['gmail'];
    $phpmailer->FromName   = $mail_title;
});

/**
 *  Color paletes
 */

add_action('wp_head', 'w_colors', 100);
function w_colors() { 
    $colors = get_field('pallete','option');
    $primary_color = $colors['primary'];
    $warning_color = $colors['warning'];
    $danger_color = $colors['danger'];
    $success_color = $colors['success'];
    $orange_color = $colors['orange'];
?>
    <style>
        :root {
            --primary: <?= ($primary_color) ? $primary_color : '#007bff' ?>;
            --warning: <?= ($warning_color) ? $warning_color : '#ffc107' ?>;
            --danger: <?= ($danger_color) ? $danger_color : '#dc3545' ?>;
            --success: <?= ($success_color) ? $success_color : '#28a745' ?>;
            --orange: <?= ($orange_color) ? $orange_color : '#fd7e14' ?>;
        }
        .btn-primary, .wp-block-button .wp-block-button__link {
            background-color: var(--primary);
            border-color: var(--primary);
        }
    </style>
<?php }

/**
 *  Breadcrumb
 * 
 */
function the_breadcrumb() {

    if (!is_front_page()) {
	
        echo '<nav aria-label="breadcrumb"><ol class="breadcrumb">';
        echo ' <li class="breadcrumb-item"><a href="';
        echo get_option('home');
        echo '">';
        echo "Trang chủ";
        echo '</a></li>';
	
        if (is_category() || is_single() || is_tax() ){
            echo '<li class="breadcrumb-item">'.get_the_archive_title().'</li>';
        } elseif (is_archive() || is_single()){
            if ( is_day() ) {
                printf( __( '%s', 'text_domain' ), get_the_date() );
            } elseif ( is_month() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
            } elseif ( is_year() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
            } else {
                _e( 'Blog Archives', 'text_domain' );
            }
        }
	
        if (is_single()) {
            echo '<li class="breadcrumb-item">'.get_the_title().'</li>';
        }
	
        if (is_page()) {
            echo '<li class="breadcrumb-item">'.get_the_title().'</li>';
        }
	
        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) { 
                $post = get_post($page_for_posts_id);
                setup_postdata($post);
                echo '<li class="breadcrumb-item">'.get_the_title().'</li>';
                rewind_posts();
            }
        }

        echo '</ol></nav>';
    }
}

/**
 *  Custom get_the_archive_title()
 * 
 */
function w_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }
  
    return $title;
}
 
add_filter( 'get_the_archive_title', 'w_archive_title' );

/**
 *  Ajax get portfolios by term slug
 * 
 */

add_action('wp_ajax_get_ports', 'ajax_get_portfolio_by_slug');
add_action('wp_ajax_nopriv_get_ports', 'ajax_get_portfolio_by_slug');
function ajax_get_portfolio_by_slug() {
    $term_slug = isset($_POST['term_slug']) ? $_POST['term_slug'] : '';
    $args = array(
        'post-type' => 'du-an',
        'tax_query' => array(
            array (
                'taxonomy' => 'linh-vuc',
                'field' => 'slug',
                'terms' => $term_slug,
            )
        ),
    );
    $query = new WP_Query( $args );        
    if ( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post(); ?>
            <div class="col-md-6 col-lg-4">
                <article class="item box-item ">
                    <a href="<?php the_permalink(); ?>" class="img-wrap link-wrap">
                        <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title(); ?>">
                    </a>
                    <a href="<?php the_permalink(); ?>">
                        <h3><?= get_the_title(); ?></h3>
                    </a>
                    <p class="decs"><?= ($except = get_the_excerpt()) ? wp_trim_words( $except, 30, '...' ) : '' ?></p>
                </article>
            </div>
        <?php endwhile;
    endif;                       
    wp_reset_postdata();

    die(); 
}
