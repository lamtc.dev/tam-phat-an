<?php

add_action( 'login_enqueue_scripts', 'load_admin_style' );
add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
    wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/assets/css/w-admin.css', false, '1.0.0' );
}
if ( wp_get_current_user()->user_email != 'weableteam@gmail.com' ) {
    function wpdocs_remove_menus(){
  
        remove_menu_page( 'tools.php' ); 
        remove_menu_page( 'wpseo_dashboard' ); 
        remove_menu_page( 'loco' ); 
        remove_menu_page( 'edit.php?post_type=acf-field-group' ); 
        remove_menu_page( 'plugins.php' ); 
        remove_menu_page( 'themes.php' ); 
        remove_menu_page( 'woocommerce'); 
        remove_menu_page( 'users.php'); 
        remove_menu_page( 'wpcf7'); 
        remove_menu_page( 'edit-comments.php'); 
        remove_menu_page( 'upload.php'); 
        remove_menu_page( 'wc-admin'); 

        remove_submenu_page( 'options-general.php','facetwp' ); 
        remove_submenu_page( 'options-general.php','options-privacy.php' ); 
        remove_submenu_page( 'options-general.php','options-media.php' ); 
        remove_submenu_page( 'options-general.php','options-discussion.php' ); 
        remove_submenu_page( 'options-general.php','options-writing.php' ); 
        remove_submenu_page( 'options-general.php','options-permalink.php' ); 
        remove_submenu_page( 'options-general.php','options-reading.php' ); 
        remove_submenu_page( 'options-general.php','ic_reviews' ); 
        remove_submenu_page( 'index.php','update-core.php' ); 

    }
    add_action( 'admin_init', 'wpdocs_remove_menus' );

    // function w_register_admin_page() {
    //     add_menu_page(
    //         __( 'Quản lý đơn hàng', 'fusshi' ),
    //         __( 'Quản lý đơn hàng', 'fusshi' ),
    //         'manage_options',
    //         'edit.php?post_type=shop_order',
    //         '',
    //         'dashicons-cart',
    //         99
    //     );

    //     add_menu_page(
    //         __( 'Xem data booking', 'fusshi' ),
    //         __( 'Xem data booking', 'fusshi' ),
    //         'manage_options',
    //         'view-data-booking',
    //         'xem_data_callbacks',
    //         'dashicons-media-spreadsheet',
    //         99
    //     );
    // }
    // add_action('admin_menu', 'w_register_admin_page');
    
    function remove_dashboard_widgets() {
        global $wp_meta_boxes;
        unset($wp_meta_boxes['dashboard']);
    }
    add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );


    function w_admin_bar_render() {

        global $wp_admin_bar;
        
        $wp_admin_bar->remove_menu('comments');
        $wp_admin_bar->remove_menu('updates');
        $wp_admin_bar->remove_menu('wp-logo');
        $wp_admin_bar->remove_menu('new-content');
        $wp_admin_bar->remove_menu('wpseo-menu');
        $wp_admin_bar->remove_node('view-store');
        $wp_admin_bar->remove_node('archive');
      
        if(current_user_can('administrator')) { 

            $wp_admin_bar->add_menu( array(
                'id' => 'w-xembooking',
                'title' => __('Xem data booking'),
                'meta' => array('target'=>'_blank'),
                'href' => 'https://docs.google.com/spreadsheets/d/1Pd_3dnNr1oIeJXAQuhoXF3c3gqfVFOEP_8h23k08vGM/edit?usp=sharing'
            ));

            $wp_admin_bar->add_menu( array(
            'id' => 'w-huongdanquantri',
            'title' => __('Hướng dẫn quản trị website'),
            'meta' => array('target'=>'_blank'),
            'href' => 'https://docs.fusshi.com/'
            ));

        } 
    }
    add_action( 'wp_before_admin_bar_render', 'w_admin_bar_render' );
}