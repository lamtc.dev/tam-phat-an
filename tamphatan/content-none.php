<?php
/**
 * The template for displaying "not found" content in the Blog Archives
 */

$search_enabled = get_theme_mod( 'search_enabled', '1' ); // Get custom meta-value
?>
<article id="post-0" class="post no-results not-found mb-5">
	<div class="entry-content">
		<?=  ( '1' === $search_enabled ) ? get_search_form() : "" ?>
		<div class="alert alert-danger mt-3" role="alert">
			Chưa có bài viết, vui lòng quay lại sau.
		</div>
	</div><!-- /.entry-content -->
</article><!-- /#post-0 -->
