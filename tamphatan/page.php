<?php
/**
 * Template Name: Page (Default)
 * Description: Page template with Sidebar on the left side
 *
 */

get_header();
?>
	<?= get_template_part('template-parts/element','page_header') ?>

	<div class="page-content-wrapper">
		<div class="container">
			<?php if (have_posts()) :
				while ( have_posts() ) :
					the_post();

					the_content();
				endwhile;
			else :
				get_template_part( 'content', 'none' );
			endif;
			?>
		</div>
	</div>
<?php
get_footer();
