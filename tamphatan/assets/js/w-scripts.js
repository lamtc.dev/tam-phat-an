( function ( $ ) {

	$('.w-m-menu').on( 'click', function () {
		$('.m-menu-wrap').toggleClass("show");
	});

    $('.m-menu-wrap .m-close').on( 'click', function () {
		$('.m-menu-wrap').removeClass("show");
	});

	$('.feedback-slider').flickity({
		// options
		cellAlign: 'left',
		contain: true,
		groupCells: '33%',
		imagesLoaded: true,
		wrapAround: true,
		percentPosition: true,
		prevNextButtons: false
	});

	$('.client-slider').flickity({
		// options
		cellAlign: 'left',
		contain: true,
		groupCells: '20%',
		imagesLoaded: true,
		wrapAround: true,
		percentPosition: false,
		prevNextButtons: false,
		pageDots: false,
		autoPlay: 1000,
		freeScroll: true,
	});

}( jQuery ) );