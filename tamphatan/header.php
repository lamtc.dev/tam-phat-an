<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php if(get_field('favicon','option')) : ?>
	<link rel="shortcut icon" type="image/x-icon" href="<?= get_field('favicon','option') ?>" />
	<?php endif; ?>

	<?php wp_head(); ?>

	<?php
    if (get_field('code_embed_head', 'option'))
        the_field('code_embed_head', 'option');
    ?>
	
</head>

<?php
	$navbar_scheme   = get_theme_mod( 'navbar_scheme', 'navbar-light bg-light' ); // Get custom meta-value.
	$navbar_position = get_theme_mod( 'navbar_position', 'static' ); // Get custom meta-value.

	$search_enabled  = get_theme_mod( 'search_enabled', '1' ); // Get custom meta-value.
?>

<body <?php body_class(); ?>>

<?php
if (get_field('code_after_body_open', 'option'))
	the_field('code_after_body_open', 'option');
?>

<?php wp_body_open(); ?>

<a href="#main" class="sr-only sr-only-focusable"><?php esc_html_e( 'Skip to main content', 'tamphatan' ); ?></a>

<div id="wrapper">
	<header class="w-header">
		<div class="top-header">
			<div class="container">
				<div class="row py-2 top-header align-items-center">
					<div class="col-md-2 text-center text-md-left">
						<a class="logo" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<?php
								$header_logo = get_field('logo', 'option'); // Get custom meta-value.

								if ( ! empty( $header_logo ) ) :
							?>
								<img src="<?php echo $header_logo; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" class="img-fluid h-100"/>
							<?php
								else :
									echo esc_attr( get_bloginfo( 'name', 'display' ) );
								endif;
							?>
						</a>
					</div>
					<div class="d-none d-md-block col-md-10 col-lg-8">
						<?php 
							if ( '1' === $search_enabled ) :
							?>
									<form class="w-search form-inline search-form my-2 my-lg-0 justify-content-center" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
										<div class="input-group">
											<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'Nhập từ khóa tìm kiếm...', 'tamphatan' ); ?>" title="<?php esc_attr_e( 'Search', 'tamphatan' ); ?>" />
											<div class="input-group-append">
												<button type="submit" name="submit" class="btn btn-primary"><?php esc_html_e( 'Tìm kiếm', 'tamphatan' ); ?></button>
											</div>
										</div>
									</form>
							<?php
							endif;
						?>
					</div>
					<div class="d-none d-lg-block col-lg-2">
						<div class="w-hotline">
							<div class="icon">
								<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone-forward-fill" viewBox="0 0 16 16">
									<path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zm10.761.135a.5.5 0 0 1 .708 0l2.5 2.5a.5.5 0 0 1 0 .708l-2.5 2.5a.5.5 0 0 1-.708-.708L14.293 4H9.5a.5.5 0 0 1 0-1h4.793l-1.647-1.646a.5.5 0 0 1 0-.708z"/>
								</svg>
							</div>
							<span class="title">Hotline tư vấn</span>
							<a href="" class="num"><?= ($GLOBALS['global_hotline']) ? $GLOBALS['global_hotline'] : '0842 94 3333' ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wrap-menu">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="main-menu d-none d-lg-block">
							<?php 
								wp_nav_menu(
									array(
										'theme_location' => 'main-menu',
										'container'      => '',
										'menu_class'	 => 'd-flex m-0 p-0',
										'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
										'walker'         => new WP_Bootstrap_Navwalker(),
									)
								);
							?>
						</div>
						<div class="main-menu-mobile d-flex d-lg-none py-2">
							<div class="left d-flex align-items-center text-white" >
								Hotline hỗ trợ: <a href="" class="text-white ml-2">0905 179 179</a>
							</div>
							<div class="right ml-auto">
								<button class="w-m-menu">
									<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" role="img" focusable="false"><title>Menu</title><path stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M4 7h22M4 15h22M4 23h22"></path></svg>
								</button>
							</div>
						</div>
						<div class="m-menu-wrap d-block d-lg-none ">
							<?php 
								wp_nav_menu(
									array(
										'theme_location' => 'main-menu',
										'container'      => '',
										'menu_class'	 => 'list-unstyled m-0 p-0',
										'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
										'walker'         => new WP_Bootstrap_Navwalker(),
									)
								);
							?>
							<button class="m-close">
								<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
									<path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z"/>
								</svg>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<main id="main" class="main <?= (is_front_page()) ? 'mt-0' : '' ?>"<?php if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' style="padding-top: 100px;"'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' style="padding-bottom: 100px;"'; endif; ?>>
		