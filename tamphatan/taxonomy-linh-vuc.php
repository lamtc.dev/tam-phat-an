<?php
/**
 * The Template for displaying Category Archive pages.
 */

get_header();

	echo get_template_part('template-parts/element','page_header');

	echo '<div class="container mt-5">';
	if ( have_posts() ) :
		get_template_part( 'archive', 'loop' );

	else :
		get_template_part( 'content', 'none' );

	endif;

	wp_reset_postdata(); // End of the loop.
	echo '</div>';
get_footer();
